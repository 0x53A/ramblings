# Fable and TypeScript - Love Story or Forbidden Affair?

Fable is just awesome. Elmish is a revelation. But is it _enough_? Can I stay happily in the walled F# garden? Should I? And how high are these walls anyway?


Wouldn't it be a safer choice to use TypeScript? Microsoft is using it. And Google. Many libraries are written in TS; most of the others have bindings available.

---------------

When I started with Fable, it did feel a bit like an island. Sure, there are many "blessed" bindings in-box. Browser APIs. React. Etc. What if I need _more_?

There is TS2Fable. The first lib without official bindings I tried it on, it just crapped itself.
Okay, maybe that wasn't fair. I tried it on Babylon.js, a complex 3D and Game Engine.

The type definition file is 1.8 MB with ~40k lines.

Just for the definition, not the implementation.

But what do I do now? Should I just burn my Fable Proof-Of-Concept and start with Typescript?

------------

No. Why should it even __be__ a choice between TS and Fable? Why not both?

How hard can it even be to embed TS components in Fable?

----------

As it turns out, pretty easy. And everything works great.

-----------------------

So it was pretty much impossible to use BabylonJS from Fable without a big investment.

I'm not gonna translate that file by hand. I had started manually fixing up the output from TS2Fable, but got stuck.

If I wanted to stay purely in F# land, my best plan would have been to fix up TS2Fable and teach it the missing conventions used by BabylonJS.

Which would probably have taken one or many weeks. Unfamiliar codebase, and I'm not even fluent in TS ...

----------------

Using BabylonJS from TypeScript, on the other hand, is easy.  
Here is a quick example for how to create a React component: https://doc.babylonjs.com/resources/babylonjs_and_reactjs  
There is also a dedicated React wrapper: https://www.npmjs.com/package/react-babylonjs  

But for my simple tests I went with the first.

----------

So how do I start? When using npm libraries I don't ``import`` any TypeScript - The library author runs TSC, and packages up the Javascript + TSD. I write the import, Webpack pulls the JS files from ``node_modules``.

How does this Webpack even work?

Duck-Dock-Going for ``typescript webpack`` showed me [awesome-typescript-loader](https://github.com/s-panferov/awesome-typescript-loader).

To keep a short story short, just add the loader and typescript itself yia ``yarn`` (or ``npm`` if you are old-school), add a ``tsconfig.json``, add a few lines to your ``webpack.config.js``, and shit works.

A working sample project is here: https://github.com/0x53A/FableTsBabylonJsSample

You can drop a loose ``.ts`` file into your project and ``ofImport`` it from F#. Webpack does the rest.
The Typescript Language Service will recognize it if you double-click it in VS. The imports from node_modules work. Intellisense works.

Instead of having to write a complete wrapper over a complex library, I can now create a domain specific component in TS, which exposes just the few knobs I actually need, then write the F# wrapper by hand (in my case it was less than 10 lines).

This is the power of specialization over abstraction.


--------------

The complex parts are still handled by Fable Elmish. Overall State Handling, Navigation. Through ___isomorphic F#___ I can share code and data structures between client and server.

But I am not _restricted_. If there is anything that can't be done in Fable, it is simple to break out of the garden. And outside the garden isn't a wasteland anyway. All these horror stories were way overblown.

---------------

So yeah, I think TS and Fable work pretty well together. Note that this isn't an equal partnership.
You could say Fable is the Dom and TS is the Sub.


While I can easily import TS modules from Fable, I'm not quite sure how to do the reverse.

Would I need to split my project up into multiple projects, one for each module I want to import in TS? Like the other way, I would also have to manually declare the types - Fable doesn't (yet) create TypeScript bindings.

But that is another topic for a different blogger ;D

__[[EDIT]]__ A working example of how to import a Fable module from TypeScript is here: https://github.com/wrattler/wrattler/pull/1/files.
Thanks @mmangel!