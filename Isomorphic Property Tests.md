So I have some Fable code. It is supposed to sort a list. But somehow it doesn't work.

I added a unit test. The unit test succeeds.

Now what?

----------

My unit test project runs in .NET, so if it succeeds, but the web app fails, then it seems to be due to a difference in environments.
Maybe I did something wrong, maybe Fable has a bug, maybe a JS lib I use has a bug, ...

___Something___ is ___different___.

### Isomorphic Tests


Wouldn't it be nice to run the same tests twice, once for JS, once for .NET?

The creators of Fable thought the same: https://github.com/fable-compiler/Fable/tree/master/tests/Main

In [Main.fs](https://github.com/fable-compiler/Fable/blob/b6498728c4b0b43090aebf3f4ac48847efb8b0e4/tests/Main/Main.fs#L48-L77), it either uses Expecto for the .NET tests, or Mocha for the JS tests.

There is a [thin wrapper](https://github.com/fable-compiler/Fable/blob/b6498728c4b0b43090aebf3f4ac48847efb8b0e4/tests/Main/Util/Util.fs#L6-L25) over both frameworks so that you can write your tests only once.


### Isomorphic Property Tests

Have you heard of FsCheck? It's kinda nice.

But now, everyone is using Fable, and FsCheck doesn't yet run in the web :/

But apparently, FsCheck is based on QuickCheck from Haskell, and there are also a few QuickCheck derivates in JS :)

One of the first frameworks I found is [jsverify](https://github.com/jsverify/jsverify).

In the core, they work similar: You define a function with inputs, and return a bool.

But of course, they do differ in the implementation.

For .NET, FsCheck uses Reflection. So unless you need to test a custom type with a custom generator, you can just write your test function, and everything works.

For JS, it isn't so simple. Afaik there is no such thing as reflection, so with jsverify, you need to define your signature.

jsverify Hello World looks something like this: 

```typescript
jsc.property("(b && b) === b", jsc.bool, b => (b && b) === b);
```

The first one is just the name. Then you have a few helpers to declare the input parameters which should be generated (``jsc.bool``, ``jsc.string``, ...). Then the test function.

In FsCheck with Expecto, we would write this like 

```fsharp
testProperty "(b && b) === b" (fun b -> (b && b) = b)
```

So the _core_, the test function, is the same, but for JS we need a bit of setup.

I settled for adding a new _facade_:

```fsharp
let testProperty<'args> name (jsWrapper:('args->bool)->unit) (test:'args->bool) = ...
```

This can then be used like

```fsharp
testProperty "(b && b) === b" (fun fn -> jsc.property(!!"(b && b) === b", jsc.bool, (fun b -> !!fn(b) )) |> ignore) (fun b -> (b && b) = b)
```

Now at first, for such a simple property it can look verbose.

Let's break it down.

The second parameter is the jsWrapper:

```fsharp
(fun fn -> jsc.property(!!"(b && b) === b", jsc.bool, (fun b -> !!fn(b) )) |> ignore)
```

This takes, as a parameter, the ___core___ function (third parameter). It then repeats the name and declares the input ``jsc.bool``. In the last parameter, we take the parameter and pass it forward to our actual property test.