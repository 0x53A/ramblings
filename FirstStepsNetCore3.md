I'm really hyped for .NET Core 3

There are alpha builds available:  https://github.com/dotnet/core-sdk#installers-and-binaries

![image](/uploads/7562e06a6f7ad986f6184fa099c6a6e9/image.png)

--------

We have a medium sized winforms application that I *really* want to port to netcore.
It needs to run in a restricted environment, where updating .Net Framework is possible but really hard.
Updating windows is impossible.

This app currently still targets .NET Framework 4.0.

In the near future we will split it into a __current__ and a __legacy__ version - __legacy__ will have to stay .NET 40 for the next 10 years (Windows XP ...), __current__ will probably be .NET 4.6.1 for now.

But even with __current__, this is just a repeat. It will probably stay on 4.6.1 for many years to come, just because we can't update the machines.

Enter .NET Core. Now the Framework is just a part of the app and we can update it as we wish, as long as it stays compatible with the underlying windows version.

-----------------------------------

This app is winforms and so still on the old csproj format.

I didn't want to convert it _now_, so I built it, then just referenced the binaries from my .NET Core project:

```xml
<Project Sdk="Microsoft.NET.Sdk">

  <PropertyGroup>
    <OutputType>WinExe</OutputType>
    <TargetFramework>netcoreapp3.0</TargetFramework>
  </PropertyGroup>

  <ItemGroup>
    <FrameworkReference Include="Microsoft.DesktopUI" />
  </ItemGroup>

  <ItemGroup>
    <Reference Include="C:\bin\myApp.exe" />
  </ItemGroup>

</Project>

```

Then I just forward Main:

```c#
using System;

namespace MyAppNetCoreApp
{
    class Program
    {
        [STAThread]
        static void Main(string[] args)
        {
            MyApp.MainForm.Main(args);
        }
    }
}

```

do a ``dotnet run`` and ... crash

```
Unhandled Exception: System.IO.FileNotFoundException: Could not load file or assembly 'System.Design, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a'. Das System kann die angegebene Datei nicht finden.
```

---------

a search found https://github.com/dotnet/core-sdk/issues/79

I added ```<Reference Include="System.Design" HintPath="C:\Windows\Microsoft.NET\Framework64\v4.0.30319\System.Design.dll" />```

and ... next crash

```
Unhandled Exception: System.MissingMethodException: Method not found: 'Void System.Threading.Mutex..ctor(Boolean, System.String, Boolean ByRef, System.Security.AccessControl.MutexSecurity)'.
```

--------------


Offender:

```c#
MutexSecurity securitySettings = ...
serverMutex = new Mutex(false, mutexName, out createdNewServerMutex, securitySettings);
```

---

I got rid of ``MutexSecurity`` and ... next crash :)

```
 System.IO.FileNotFoundException: Could not load file or assembly 'System.Data.DataSetExtensions, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089'. Das System kann die angegebene Datei nicht finden.
 ```
 
 -------------
 
 Adding ```<PackageReference Include="System.Data.DataSetExtensions" />``` got me over this issue and I finally saw something! 
 
![image](/uploads/c695e909494db78f7fb673090e3f90f5/image.png)

----------

So, apparently binary serialization is not (yet?) ported.

How often is ApplyResources used, anyway?

![image](/uploads/845ca390421528df26d96fbed6138971/image.png)

Uhhh, ok.

Find and replace ``resources.ApplyResources`` -> ``//resources.ApplyResources``.

---------------

And next issue:

![image](/uploads/e504b127e0829ca58eb7cb426db8a823/image.png)

Offending code (``waitDialogThread.CurrentUICulture = Thread.CurrentThread.CurrentUICulture;`` throws):

```c#
        public void Start()
        {
            if (waitDialogThread == null)
            {
                terminateWaitDialogThread = false;
                waitDialogThread = new Thread(new ThreadStart(WaitDialogThreadFct));
                waitDialogThread.Name = "WaitDialog Thread";

                waitDialogThread.CurrentUICulture = Thread.CurrentThread.CurrentUICulture;
                waitDialogThread.CurrentCulture = Thread.CurrentThread.CurrentCulture;

                waitDialogThread.Start();
            }
        }
```

well, I don't really care about no pesky cultures. Lets remove these two lines.

---

Hitting a few more resource related issues and cutting code out ...

---

next issue:

![image](/uploads/678a8e5355b35241a2a8c160d8dd2861/image.png)

---

fixed by

```<PackageReference Include="System.Data.SqlClient" />```

----

next issue

```
Unhandled Exception: System.IO.FileNotFoundException: Could not load file or assembly 'System.Diagnostics.PerformanceCounter, Version=0.0.0.0, Culture=neutral, PublicKeyToken=cc7b13ffcd2ddd51'. Das System kann die angegebene Datei nicht finden.
```

fixed by

```
<PackageReference Include="System.Diagnostics.PerformanceCounter" />
```

----------


The last issue is this...

![image](/uploads/f1ed3fa6cf91b738c38ff74ca6ed570f/image.png)

from inside an unmaintained third-party control.

I'll probably have to either rewrite the Assembly with [dnSpy](https://github.com/0xd4d/dnSpy), or patch it at runtime with [Harmony](https://github.com/pardeike/Harmony).

If you know of a better solution, hit me up.

----------
Let's wait for it to grow more compatible, for now ...
The resource issues alone make a port almost impossible.